//
//  HomeVM.swift
//  RebuStar Rider
//
//  Created by Abservetech on 28/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import GoogleMaps

class HomeVM{
    
    //Mark :- Data Declaraction
    var dataService : ApiRoot?
    
    var view : UIView?
    
    var error: Error? {
        didSet {
            guard let error = error else { return }
            self.showErrorAlertClosure?()
        }
    }
    
    var vechileservice : VechileServiceModel?{
        didSet{
            guard let vechile = vechileservice else { return }
            self.getVechileClosure?()
        }
    }
    var errVechileservice : VechileServiceModel?{
        didSet{
            guard let error = error else { return }
         //   showToast(msg: "\(errVechileservice?.message ?? "")!!!!" + Localize.stringForKey(key: "try_again_later"))
            self.errgetVechileClosure?()
        }
    }
    
    var fareDetail : EstimateFareDetails?{
        didSet{
            guard let fare = fareDetail else { return }
            self.getFareClouser?()
        }
    }
    
    var errfareDetail : EstimateFareDetails?{
        didSet{
            guard let errfare = errfareDetail else { return }
           // showToast(msg: "\(errfareDetail?.message ?? "")!!!!" + Localize.stringForKey(key: "try_again_later"))
            self.errgetFareClouser?()
        }
    }
    
    var request : RideRequestModel?{
        didSet{
            guard let response = request else { return }
            getRequestClouser?()
        }
    }
    
    var errrequest : RideRequestModel?{
        didSet{
            guard let err = errrequest else { return }
            errRequestClouser?()
            //showToast(msg: err.message ?? "")
        }
    }
    
    var cancelRequest : RideRequestModel?{
        didSet{
            guard let response = cancelRequest else { return }
            getcancelClouser?()
        }
    }
    
    var errcancelRequest : RideRequestModel?{
        didSet{
            guard let err = errcancelRequest else { return }
            errcancelClouser?()
            showToast(msg: err.message ?? "")
        }
    }
    
    var cancelTrip : cancelTripModel?{
        didSet{
            guard let response = cancelTrip else { return }
            getcancelTripClouser?()
        }
    }
    
    var errcancelTrip : cancelTripModel?{
        didSet{
            guard let err = errcancelTrip else { return }
            errcancelTripClouser?()
            showToast(msg: err.message ?? "")
        }
    }
    
    
    var rideDetails : RideDetailModel?{
        didSet{
            guard let response = rideDetails else { return }
            getRideDetailClouser?()
        }
    }
    
    var errrideDetails : RideDetailModel?{
        didSet{
            guard let err = errrideDetails else { return }
            errRideDetailClouser?()
            showToast(msg: err.message ?? "")
        }
    }
    var feedback : FeedBackModel?{
        didSet{
            guard let response = feedback else { return }
            getfeedbackClouser?()
        }
    }
    
    var errfeedback : FeedBackModel?{
        didSet{
            guard let err = errfeedback else { return }
            errfeedbackClouser?()
            showToast(msg: err.message ?? "")
        }
    }
    
    var promoSuccess : String?{
        didSet{
            guard let promo = promoSuccess else { return }
            promosuccess?()
        }
    }
    
    var errpromoSuccess : String?{
        didSet{
            guard let promo = errpromoSuccess else { return }
            errpromo?()
        }
    }
    
    //Mark :- Constructor
    init() { }
    
    init(view : UIView ,dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    init(dataService : ApiRoot) {
        self.dataService = dataService
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var errorinFetchData: (() -> ())?
    var showErrorAlertClosure: (() -> ())?
    var getVechileClosure: (()->())?
    var errgetVechileClosure: (()->())?
    var getFareClouser : (() -> ())?
      var errgetFareClouser : (() -> ())?
    var getRequestClouser : (() -> ())?
    var errRequestClouser : (()->())?
    var getcancelTripClouser : (() -> ())?
    var errcancelTripClouser : (()->())?
    var getcancelClouser : (() -> ())?
    var errcancelClouser : (()->())?
    var getRideDetailClouser : (() -> ())?
    var errRideDetailClouser : (()->())?
    var getfeedbackClouser : (() -> ())?
    var errfeedbackClouser : (()->())?
    var promosuccess : (()->())?
    var errpromo : (()->())?
    
    // MARK: - Network call
    func getVechileList(view : UIView , pickupLoc : CLLocation , dropLoc : CLLocation  ){
        let url = ServiceApi.vehicleServe
        
        var params = Parameters()
        params["pickupLat"] = "\((pickupLoc.coordinate.latitude ?? 0.0).description)"
        params["pickupLng"] = "\((pickupLoc.coordinate.longitude ?? 0.0).description)"
        params["dropLat"] = "\((dropLoc.coordinate.latitude ?? 0.0).description)"
        params["dropLng"] = "\((dropLoc.coordinate.longitude ?? 0.0).description)"
        params["tripType"] = "daily"
        
        self.dataService?.postApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.vechileservice = VechileServiceModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errVechileservice = VechileServiceModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func getextimateFare(view : UIView , pickupLoc : CLLocation , dropLoc : CLLocation , serviceDetail : VechileListData,pickupCity : String){
        
        let url = ServiceApi.estimationFare
        
        var params = Parameters()
        params["promoCode"] = UserDefaults.standard.string(forKey: UserDefaultsKey.promo)?.uppercased()
//        UserDefaults.standard.removeObject(forKey: UserDefaultsKey.promo)
        
        params["serviceType"] = serviceDetail.type
        params["serviceTypeId"] = serviceDetail._id
        params["time"] = ""
        params["pickupLat"] = "\((pickupLoc.coordinate.latitude ?? 0.0).description)"
        params["pickupLng"] = "\((pickupLoc.coordinate.longitude ?? 0.0).description)"
        params["pickupCity"] = pickupCity
        params["dropLat"] = "\((dropLoc.coordinate.latitude ?? 0.0).description)"
        params["dropLng"] = "\((dropLoc.coordinate.longitude ?? 0.0).description)"
        params["tripType"] = "daily"
        params["hotelId"] = ""
        
        self.dataService?.postApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.fareDetail = EstimateFareDetails(fromJson: convertToDictionary(text: success.description) ?? ["":""])
           
        }, jsonError: { (jsonError) in
            self.errfareDetail = EstimateFareDetails(fromJson: convertToDictionary(text: jsonError.description) ?? ["":""])
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func sendRideRequest(view : UIView ,date : String ,paymentType : String , pickupCity : String , bookingtype : String ,tripTime : String, estimateFare : EstimateFareDetails,utc : String){
        if let fare : EstimateFareDetails = estimateFare{
          
            let url = ServiceApi.requestTaxi
            print("*** Payment Type : ",paymentType)
            var params = Parameters()
            params["vehicleDetailsAndFare"] = ""
            params["distanceDetails"] = ""
            params["promo"] = UserDefaults.standard.value(forKey: UserDefaultsKey.promo) as? String ?? ""
            UserDefaults.standard.removeObject(forKey: UserDefaultsKey.promo)
            params["promoAmt"] = ""
            params["tripType"] = "daily"
            params["tripDate"] = date
            params["paymentMode"] = paymentType
            params["pickupCity"] = pickupCity
            params["requestFrom"] = "app"
            params["bookingType"] = bookingtype
            params["serviceType"] = fare.vehicleDetailsAndFare.vehicleDetails.type ?? ""
            params["estimationId"] = fare.estimationId
            params["tripTime"] = tripTime
            params["notesToDriver"] = ""
            params["bookingFor"] = ""
            params["otherPh"] = ""
            params["otherPhCode"] = Constant.phoneCode
            params["noofseats"] = "1"
            params["utc"] = utc
            
            self.dataService?.postApi(view: view, url: url, params: params, jsonSuccess: { (success) in
                self.request = RideRequestModel(json: success)
                
            }, jsonError: { (jsonError) in
                self.errrequest = RideRequestModel(json: jsonError)
            }, error: { (Error) in
                print("@@@Error" ,Error)
            }, dataSuccess: { (responseData) in
                print("@@@responseData" ,responseData)
            },dataError: { (errorData) in
                print("@@@errorData" ,errorData)
            })
        }
    }
    
    
    func cancelRide(view : UIView ,requestid : String){
        let url = ServiceApi.cancelTaxi
        
        var params = Parameters()
        params["requestId"] = requestid
      
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
           self.cancelRequest = RideRequestModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errcancelRequest = RideRequestModel.init(json: jsonError)
          }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    
    
    func cancelCurrentTrip(view : UIView ,tripId : String){
        let url = ServiceApi.cancelCurrentTrip
        
        var params = Parameters()
        params["tripId"] = tripId
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.cancelTrip = cancelTripModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errcancelTrip = cancelTripModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
    }
    
    func tripDriverDetails(view : UIView , tripId : String){
        let url = ServiceApi.tripDriverDetails
       
        var params = Parameters()
        params["tripId"] = tripId
        
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.rideDetails = RideDetailModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errrideDetails = RideDetailModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
        
    }
    
    func riderFeedBack(view : UIView , tripId : String,rating : String ,comments : String ){
        let url = ServiceApi.riderFeedback
        
        var params = Parameters()
        params["tripId"] = tripId
        params["rating"] = rating
        params["comments"] = comments
        
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            self.feedback = FeedBackModel.init(json: success)
        }, jsonError: { (jsonError) in
            self.errfeedback = FeedBackModel.init(json: jsonError)
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
        
    }
    
    func validatePromo(view : UIView , code : String ){
        let url = ServiceApi.validatePromo
        
        var params = Parameters()
        params["promoCode"] = code.uppercased()
        
        
        self.dataService?.putApi(view: view, url: url, params: params, jsonSuccess: { (success) in
            print("@@@responseJSON" ,success)
            self.promoSuccess = success["message"] as? String ?? "success"
            
        }, jsonError: { (jsonError) in
             print("@@@errorJSON" ,jsonError)
            self.errpromoSuccess = "Promo Code is not applicable."
        }, error: { (Error) in
            print("@@@Error" ,Error)
        }, dataSuccess: { (responseData) in
            print("@@@responseData" ,responseData)
        },dataError: { (errorData) in
            print("@@@errorData" ,errorData)
        })
        
    }
}



