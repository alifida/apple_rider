//
//  WalletVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 31/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import Lottie
import WebKit
import Alamofire

class WalletVC: UIViewController,WKNavigationDelegate {

    
    
    @IBOutlet weak var yourblanceTitle: UILabel!
    
    
    @IBOutlet weak var balcnceLbl: UILabel!
    
    @IBOutlet weak var addmoneyTitle: UILabel!
    
    @IBOutlet weak var saftytxtLbl: UILabel!
    
    @IBOutlet weak var viewTransactinBtn: UIButton!
    @IBOutlet weak var addmoneyBtn: UIButton!
    @IBOutlet weak var privacytxtlbl: UILabel!
    @IBOutlet weak var billtxt: UILabel!
    @IBOutlet weak var btn100: UIButton!
    @IBOutlet weak var btn50: UIButton!
    @IBOutlet weak var btn25: UIButton!
    @IBOutlet weak var rechargeTxF: UITextField!
    @IBOutlet weak var rechargeView: UIView!
    @IBOutlet weak var lottie: UIView!
     @IBOutlet weak var paymoneyView: UIView!
     @IBOutlet weak var closeImage: UIImageView!
     @IBOutlet weak var closeView: UIView!
    var webView = WKWebView()

   
    
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    var paymentvm = PaymentVM()
    var animationViewLarge = AnimationView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getWalletMoney()
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupView()
        self.paymentvm = PaymentVM(dataService: ApiRoot())
        self.setupAction()
        self.setupLang()
        self.setupData()
    }
    
    func setupView(){
        
//        self.closeImage.isRoundedView = true
        self.closeView.isRoundedBorder = true
        
        self.barButtonItem(ViewController: self, title: Localize.stringForKey(key: "mywallet"))

        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.revealViewController().rearViewRevealWidth = 300
        self.animationViewLarge = AnimationView(name: "wallet")
        let starbuildingAnimation = Animation.named("wallet")
        self.animationViewLarge.animation = starbuildingAnimation
        self.animationViewLarge.animationSpeed = 0.9
        self.animationViewLarge.loopMode = .loop
        self.animationViewLarge.frame = CGRect(x: 0, y: 0, width: self.lottie.frame.width, height: self.lottie.frame.height)
        self.lottie.addSubview(self.animationViewLarge)
        self.lottie.clipsToBounds = true
        self.lottie.layer.masksToBounds = true
//        self.btn25.roundeCornorBorder = 10
//        self.btn50.roundeCornorBorder = 10
//        self.btn100.roundeCornorBorder = 10
//        self.addmoneyBtn.roundeCornorBorder = 20
//        self.viewTransactinBtn.roundeCornorBorder = 20
        self.rechargeView.isElevation = 4
        
        rechargeTxF.placeholder = Localize.stringForKey(key: "recharge_amt")
    }
    
    func setupAction(){
        self.closeImage.addAction(for: .tap) {
            self.paymoneyView.isHidden = true
             self.getWalletMoney()
        }
        self.btn25.addAction(for: .tap) {
            self.rechargeTxF.text = "25"
        }
        self.btn50.addAction(for: .tap) {
            self.rechargeTxF.text = "50"
            
        }
        self.btn100.addAction(for: .tap) {
            self.rechargeTxF.text = "100"
            
        }
        
        self.privacytxtlbl.addAction(for: .tap) {
            TermsConditionView.getView.initView(view: self.view)
        }
        
        self.addmoneyBtn.addAction(for: .tap) {
            let balace : String = self.rechargeTxF.text ?? "0.0"
            if !balace.isEmpty{
                self.addMobileMoney(rechargeAmount: balace)
            }else{
                showToast(msg: self.Localize.stringForKey(key: "enter_money"))
            }
        }
        
        self.viewTransactinBtn.addAction(for: .tap) {
            let vc = ViewTranscationVC.initWithStory()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func setupData(){
     
    }
    
    func setupLang(){
        self.yourblanceTitle.text = self.Localize.stringForKey(key: "your_blnc")
        self.addmoneyTitle.text = self.Localize.stringForKey(key: "add_money")
        self.saftytxtLbl.text = self.Localize.stringForKey(key: "safty")
        self.billtxt.text = self.Localize.stringForKey(key: "proceed")
        self.privacytxtlbl.text = self.Localize.stringForKey(key: "view_tc")
        self.addmoneyBtn.setTitle(self.Localize.stringForKey(key: "add_money"), for: .normal)
        self.viewTransactinBtn.setTitle(self.Localize.stringForKey(key: "view_tran"), for: .normal)
    }

    class func initWithStory()->WalletVC{
        let vc = UIStoryboard.init(name: "Wallet", bundle: Bundle.main).instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
        return vc
    }
}
extension WalletVC{
    func addmoney(addmoney : String){
        
        self.paymentvm.addToMyWallet(view: self.view, rechargeAmount: addmoney)
                                     
        self.paymentvm.successwallet = {
            self.getWalletMoney()
            showToast(msg: self.Localize.stringForKey(key: "money_added_successfully"))
        }
    }
    
    func addMobileMoney(rechargeAmount: String){
        let mobile = Constant.profileData.phone
//        self.paymentvm.weeCash(view : self.view , rechargeAmount: rechargeAmount ,mobile :mobile)
      
        self.webView = WKWebView(frame: CGRect(x: 10, y: 100, width: self.paymoneyView.frame.size.width-20, height: self.paymoneyView.frame.size.height-120))
        self.paymoneyView.addSubview(webView)
        webView.navigationDelegate = self
        ShowMsginWindow.instanse.LoadingShow(view: self.view)
        
        if  NetworkReachabilityManager()!.isReachable {
            ShowMsginWindow.instanse.LoadingHide(view: self.view)
            let url = URL(string: ServiceApi.mobileMoney + "/\(mobile)/\(rechargeAmount)/wallet")
            print("ssss\(ServiceApi.mobileMoney + "/\(mobile)/\(rechargeAmount)")")
            webView.load(URLRequest(url: url!))
             self.paymoneyView.isHidden = false
            self.webView.isHidden = false
            self.rechargeTxF.text = ""
        }else{
            ShowMsginWindow.instanse.LoadingHide(view: self.view)
            self.webView.isHidden = true
             self.paymoneyView.isHidden = true
        }
        
    }
    
    func getWalletMoney(){
        self.paymentvm.getMyWallet(view: self.view)
        self.paymentvm.successwallet = {
            self.balcnceLbl.text = decimalDataString(data: self.paymentvm.walletData?.balance.description ?? "0")
            
        }
    }
}
