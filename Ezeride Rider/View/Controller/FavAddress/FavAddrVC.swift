//
//  FavAddrVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 23/06/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import GoogleMaps

protocol FavProtocol {
    func selectedAddres(address : String,location : CLLocation)
}

class FavAddrVC: UIViewController,FavProtocol {
    
    func selectedAddres(address: String, location: CLLocation) {
        self.fullView.isHidden = false
        self.favView.roundeCornorBorder = 3
        self.newAddress = address
        self.newLocation = location
        self.offcImg.image = UIImage(named: "ic_tick_green")
        self.addressTitleTXF.text = Localize.stringForKey(key: "office")
        self.addressLbl.text = address
    }
    
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var favAddressTable: UITableView!
    
    @IBOutlet weak var favView: UIView!
   
    @IBOutlet weak var offcLbl: UILabel!
     @IBOutlet weak var homeLbl: UILabel!
     @IBOutlet weak var otherLbl: UILabel!
    
    @IBOutlet weak var offcBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
   
    @IBOutlet weak var addressLbl: UILabel!
  
    @IBOutlet weak var addressTitleTXF: UITextField!
   
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var offcImg: UIImageView!
    @IBOutlet weak var homeImg: UIImageView!
    @IBOutlet weak var otherImg: UIImageView!
    //initilaze Variable
    let Localize : Localizations = Localizations.instance
    var favAddrvm = CommonVM()
    var newAddress : String = String()
    var newLocation : CLLocation = CLLocation()
    var favAddressList : FavAddrModel?{
        didSet{
            if let favaddr = self.favAddressList{
                self.favAddressTable.reloadData()
                  self.fullView.isHidden = true
            }
        }
    }
    
    
   override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.favAddrvm = CommonVM(view: self.view, dataService: ApiRoot())
        self.setupview()
        self.setupAction()
        self.setupLang()
        self.setupData()
        self.setupDelegate()
        self.getAddress()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
    
    
    class func initWithStory()->FavAddrVC{
        let vc = UIStoryboard.init(name: "FavAddre", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavAddrVC") as! FavAddrVC
        return vc
    }
    
    func setupAction(){
        self.saveBtn.addAction(for: .tap) {
            if !self.newAddress.isEmpty{
                let titlestr : String = self.addressTitleTXF.text ?? ""
                if titlestr.isEmpty{
                    showToast(msg: self.Localize.stringForKey(key: "address_title"))
                }else{
                    
                    self.addAddress(name: titlestr , address: self.newAddress, lat: self.newLocation.coordinate.latitude.description, lng: self.newLocation.coordinate.longitude.description)
                }
            }else{
                showToast(msg: self.Localize.stringForKey(key: "address_type"))
            }
        }
        self.cancelBtn.addAction(for: .tap) {
            self.newLocation = CLLocation()
            self.newAddress = ""
            self.fullView.isHidden = true
        }
        self.homeBtn.addAction(for: .tap) {
            if  self.homeImg.image == UIImage(named: "ic_tick_green"){
                 self.homeImg.image = UIImage(named: "ic_tick_grey")
            }else{
                self.homeImg.image = UIImage(named: "ic_tick_green")
            }
            self.offcImg.image  = UIImage(named: "ic_tick_grey")
            self.otherImg.image  = UIImage(named: "ic_tick_grey")
            self.addressTitleTXF.text = self.Localize.stringForKey(key: "home")
        }
        self.offcBtn.addAction(for: .tap) {
            if  self.offcImg.image == UIImage(named: "ic_tick_green"){
                self.offcImg.image = UIImage(named: "ic_tick_grey")
            }else{
                self.offcImg.image = UIImage(named: "ic_tick_green")
            }
            self.homeImg.image = UIImage(named: "ic_tick_grey")
            self.otherImg.image  = UIImage(named: "ic_tick_grey")
            self.addressTitleTXF.text = self.Localize.stringForKey(key: "office")
        }
        self.otherBtn.addAction(for: .tap) {
            if  self.otherImg.image == UIImage(named: "ic_tick_green"){
                self.otherImg.image = UIImage(named: "ic_tick_grey")
            }else{
                self.otherImg.image = UIImage(named: "ic_tick_green")
            }
            self.homeImg.image = UIImage(named: "ic_tick_grey")
            self.offcImg.image  = UIImage(named: "ic_tick_grey")
            self.addressTitleTXF.placeholder = self.Localize.stringForKey(key: "enter_text")
            self.addressTitleTXF.text = ""
        }
    }
    
    func setupLang(){
    }
    
    func setupview(){
        self.barButtonItem(ViewController: self, title: Localize.stringForKey(key: "Fav"))
        
        offcLbl.text = Localize.stringForKey(key: "office")
        homeLbl.text = Localize.stringForKey(key: "home")
        otherLbl.text = Localize.stringForKey(key: "other")
        cancelBtn.setTitle(Localize.stringForKey(key: "cancel"), for: .normal)
        saveBtn.setTitle(Localize.stringForKey(key: "save"), for: .normal)
        
        
        //EditButton View
        var rightBarButton = UIBarButtonItem()
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        rightButton.setTitle(Localize.stringForKey(key: "add"), for: .normal)
        rightButton.setTitleColor(UIColor(named: "AppColor"), for: .normal)
        rightBarButton = UIBarButtonItem(customView: rightButton)
        rightButton.addTarget(self, action: #selector(self.addFavAddress), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    func setupData(){
   
    }
    
    func setupDelegate() {
        self.favAddressTable.delegate = self
        self.favAddressTable.dataSource = self
        self.favAddressTable.register(UINib(nibName: "FavAddressCell", bundle: nil), forCellReuseIdentifier: "FavAddressCell")
        self.favAddressTable.reloadData()
    }
    
    @objc func addFavAddress(){
        self.fullView.isHidden = true
        
        let vc = AutoCompleteVC.initWithStory()
        vc.favdelegate = self
        self.present(vc, animated: true, completion: nil)
    }
}


extension FavAddrVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.favAddressList?.FavAddrList.count{
            if count > 0 {
                ShowMsginWindow.instanse.hideNodataView()
                return count
            }else{
                ShowMsginWindow.instanse.nodataView(view: self.view)
            }
        }
        ShowMsginWindow.instanse.nodataView(view: self.view)
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
           let cell = tableView.dequeueReusableCell(withIdentifier: "FavAddressCell", for: indexPath) as! FavAddressCell
            cell.deleteImage.isHidden = true
            if let addressList = self.favAddressList?.FavAddrList[indexPath.row]{
              cell.addrTitleLbl.text = addressList.lable
              cell.addressLbl.text = addressList.address
            }
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: UITableViewRowAction.Style.destructive, title: "Delete") { (action, indexPath) in
            if let addressList = self.favAddressList?.FavAddrList[indexPath.row]{
                self.deleteAddress(id: addressList._id)
                self.favAddressList?.FavAddrList.remove(at: indexPath.row)
                self.favAddressTable.reloadData()
            }
        }
        return [delete]
    }
}

// Api call
extension FavAddrVC {
    func getAddress(){
        self.favAddrvm.getFavAddrList(view: self.view)
        
        self.favAddrvm.successFavAddr = {
            self.favAddressList = self.favAddrvm.favAddrList
        }
    }
    
    func addAddress(name: String, address: String, lat: String, lng: String){
        self.favAddrvm.addFavAddrList(view: self.view, name: name, address: address, lat: lat, lng: lng)
    }
    
    func deleteAddress(id : String){
        self.favAddrvm.deleteFavAddrList(view: self.view, emgContactId: id)
    }
}
