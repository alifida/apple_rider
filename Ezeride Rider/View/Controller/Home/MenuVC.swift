//
//  MenuVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 31/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import PINRemoteImage
import GoogleSignIn
import FBSDKLoginKit

class MenuVC: UIViewController {
    
    //UIDeclaraction
    
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var logoutImage: ImageLoader!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var logoutLabel: UILabel!
    
    @IBOutlet weak var menuTabelView: UITableView!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var logoutView: UIView!
    
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var menuArray :  [String] = []
    //removed wallet after profile
    // UIImage(named: "mywallet")  ?? UIImage(),
    var menuImgArray : [UIImage] = [/*UIImage(named: "house")  ?? UIImage(),*/UIImage(named: "profile")  ?? UIImage(),UIImage(named: "payment")  ?? UIImage(),UIImage(named: "your_trips")  ?? UIImage(),UIImage(named: "marker_location")  ?? UIImage(),UIImage(named: "invite_frirnds")  ?? UIImage(),UIImage(named: "emrgncy_conct")  ?? UIImage(),UIImage(named: "support")  ?? UIImage(),UIImage(named: "offers")  ?? UIImage()]
    
   override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
//        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        self.revealViewController()?.frontViewController.view.alpha = 0.3
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
//        self.revealViewController()?.frontViewController.view.alpha = 1
    }
    
    func setupData(){
        if let profileData : ProfileModel  = Constant.profileData {
            self.userName.text = profileData.fname + " " + profileData.lname
            var urls : String = ServiceApi.Base_Image_URL+profileData.profile ?? String()
            
            self.userProfileImg?.pin_setImage(from: URL(string: urls))
            self.userProfileImg.layer.cornerRadius = self.userProfileImg.frame.width / 2
            self.userProfileImg.clipsToBounds = true
        }else{
            
        }
        

    }
    
    func setupView(){
        self.menuTabelView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        self.menuTabelView.delegate = self
        self.menuTabelView.dataSource = self
        self.menuTabelView.reloadData()
        
        //set colors
        self.userName.textColor = UIColor.black
        self.logoutImage.change_image = true
        self.logoutImage.tintColor = UIColor.gray
    }
    
    func setupAction(){
        self.profileView.addAction(for: .tap) {
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: ProfileVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        }
        
        self.logoutView.addAction(for: .tap) {
            let alert = UIAlertController(title: self.Localize.stringForKey(key: "logout"), message: self.Localize.stringForKey(key: "alert_logout"), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "cancel"), style: UIAlertAction.Style.default, handler: nil))
            alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "logout").lowercased(), style: UIAlertAction.Style.default, handler: { (alert) in
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                GIDSignIn.sharedInstance().signOut()
                let loginManager = LoginManager()
                loginManager.logOut()
                
                let navigation = UINavigationController(rootViewController: LauncherVC.initWithStoryBoard())
                self.appDelegate.window?.rootViewController = navigation
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func setupLang(){
        //removing wallet from menu array after profile
        //Localize.stringForKey(key: "mywallet"),
        
        self.menuArray = [/*Localize.stringForKey(key: "home"),*/Localize.stringForKey(key: "myprofile"),Localize.stringForKey(key: "payment"),Localize.stringForKey(key: "your_trips"),Localize.stringForKey(key: "Fav"),Localize.stringForKey(key: "invite_frirnds"),Localize.stringForKey(key: "emrg_contact"),Localize.stringForKey(key: "support"),Localize.stringForKey(key: "offers")]
        self.logoutLabel.text = Localize.stringForKey(key: "logout")
    }
    
    class func initWithStory()->MenuVC{
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        return vc
    }
}

extension MenuVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.menuLbl.text = self.menuArray[indexPath.row]
        cell.menuImg.image = self.menuImgArray[indexPath.row]
        cell.menuImg.image =  cell.menuImg.image?.withRenderingMode(.alwaysTemplate)
        cell.menuImg.tintColor = UIColor.gray
        cell.menuLbl.textColor = UIColor.black
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
//        case 0:
//            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
//           self.appDelegate.window?.rootViewController = MenuRoot
        case 0:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: ProfileVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        case 1:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: PaymentVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
            
            // commented this for wallet removal
//        case 1:
//            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: WalletVC.initWithStory()))
//            self.appDelegate.window?.rootViewController = MenuRoot
        case 2:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: YourTripsVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        case 3:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: FavAddrVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
            
        case 4:
            let MenuRoot =  SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: InviteFrndsVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        case 5:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: EmergnecyContactVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        case 6:
            let MenuRoot =  SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: SupportVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        case 7:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: OfferVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        default:
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
      }
        self.revealViewController().revealToggle(animated: true)
    }
}


