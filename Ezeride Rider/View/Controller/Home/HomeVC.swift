//
//  HomeVC.swift
//  RebuStar Rider
//
//  Created by Abservetech on 31/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import GoogleMaps
import PINRemoteImage
import MessageUI
import MarqueeLabel
import GeoFire
import Firebase


/* view hiddenstates
 1. === hide vehicle list and show from label
 2. ==  hide from lable and show vehcile list with map marker for trip route
 3.
 */

/* subview lists
 1. for add vehcile lust subview
 2. for add extimate list subview
 */

protocol TripRoutes {
    func getPickupDropLocation(pickAddr : String,pickupLoc : CLLocation , dropAddr : String , dropLoc : CLLocation)
    func getPinLocation(tag : String , addr : String , addrLoc : CLLocation)
    func clearMapView()
}

extension HomeVC : TripRoutes{
    
    func getPinLocation(tag: String, addr: String, addrLoc: CLLocation) {
        self.mapView.clear()
        if tag == "pickup"{
            self.pickupaddr = addr
            self.pickupLoc = addrLoc
            self.setPolyLineWithMaker(pickupaddr: addr, dropaddr: dropAddr, pickupLoc: addrLoc, dropLoc: dropLoc, isRideFlowStated: false,tripstatus: "")
        }else{
            self.dropAddr = addr
            self.dropLoc = addrLoc
            self.setPolyLineWithMaker(pickupaddr: pickupaddr, dropaddr: addr, pickupLoc: pickupLoc, dropLoc: addrLoc, isRideFlowStated: false,tripstatus: "")
            
        }
        
        self.requestBtn.setTitle(Localize.stringForKey(key: "request_now"), for: .normal)
    }
    
    
    func getPickupDropLocation(pickAddr: String, pickupLoc: CLLocation, dropAddr: String, dropLoc: CLLocation) {
        self.mapView.clear()
        self.pickupaddr = pickAddr
        self.pickupLoc = pickupLoc
        self.dropAddr = dropAddr
        self.dropLoc = dropLoc
        self.setPolyLineWithMaker(pickupaddr: pickAddr, dropaddr: dropAddr, pickupLoc: pickupLoc, dropLoc: dropLoc, isRideFlowStated: false,tripstatus: "")
        
        // vehcile api call
        self.getVechileServiceList(pickupLoc: pickupLoc, dropLoc: dropLoc)
        
        self.requestBtn.setTitle(Localize.stringForKey(key: "request_now"), for: .normal)
       
    }
    
    func clearMapView()  {
        self.viewDidLoad()
        self.viewWillAppear(true)
    }
}


class HomeVC: UIViewController ,MFMessageComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // Api response
    
    var vechileList : VechileServiceModel?{
        didSet{
            self.vehicleCollectionView.reloadData()
        }
    }
    
    //UI Declaraction
    
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var fromAddressLBL: UILabel!
    @IBOutlet weak var promoLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    
    @IBOutlet weak var tripStatusLbl: UILabel!
    @IBOutlet weak var requestBtn: UIButton!
    
    @IBOutlet weak var cashViewBtn: UIButton!
    
    @IBOutlet weak var promoBtn: UIButton!
    @IBOutlet weak var cashView: UIView!
  
    @IBOutlet weak var promoView: UIView!
    @IBOutlet weak var vechileView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var menusView: UIView!
    
    @IBOutlet weak var tripStatusView: UIView!
    
    @IBOutlet weak var designLbl: MarqueeLabel!
    @IBOutlet weak var sourceAddrssLbl: MarqueeLabel!
    @IBOutlet weak var sourceAddrView : UIView!
    @IBOutlet weak var designationView : UIView!
    
    @IBOutlet weak var vehicleCollectionView: UICollectionView!
    
    //RippleView
    
    @IBOutlet weak var sendRequestView: UIView!
    @IBOutlet weak var rippleView: UIView!
    @IBOutlet weak var cancelImg: UIImageView!
    
    var rippleEffectView: SMRippleView?

    //views
    let paymentView = PaymentDetailView.getView
    let promoCodeView = PromoView.getView
    let tripView = TripStatusView.getView
    
    var driverMarkers : [GMSMarker] = [GMSMarker]()
    var geoLocation : [CLLocation] = [CLLocation]()
    var geokey : [String] = [String]()
    
    //Variyable Declaraction
    
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let FBCONNECT = FireBaseconnection.instanse
    
    var currentLocation = CLLocation()
    var pickupaddr : String = ""
    var pickupCity : String = ""
    var pickupLoc : CLLocation = CLLocation()
    var dropAddr : String = ""
    var dropCity : String = ""
    var dropLoc : CLLocation = CLLocation()
    var driverMarker = GMSMarker()
    
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.delegate = self
        return _locationManager
        
    }()
    
    // variable for animated pollyline
    var timer: Timer!
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var pickupInfoWindow : PickupLocView?
    var dropInfoWindow : DropLocView?
    var pickupTagMarker = PickupMarker()
    var dropTagMarker = DropMarker()
    
    var focusZoom : Float = 12
    var selectedVCIndex : Int = -1
    var currentAddress : String = ""
    var subviewCount : Int = -1
    var hiddenShowViews : Int = 1
    var serviceData : VechileListData = VechileListData()
    var requestData : RideRequestModel = RideRequestModel()
    
    var homevm = HomeVM()
    var googleVM = GoogleVM()
    var paymentvm = PaymentVM()
    var isdrawedStartedPolyLine : Bool = false
       var isdrawedAcceptedPolyLine : Bool = false
       
    
   override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.mapView.clear()
        
        self.homevm = HomeVM(view: self.vechileView, dataService: ApiRoot())
        self.googleVM = GoogleVM(view: self.view, dataService: ApiRoot())
      
        self.setupAction()
        self.setupView()
        self.setupLang()
        self.setupDelegate()
        self.setupMapDelegate()
        self.setupCVDelegate()
        self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
        self.observeNotification()
        self.hideShowView()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.cashView.addGestureRecognizer(tap)
        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        
        self.paymentvm = PaymentVM(view: self.view, dataService: ApiRoot())
        
        //Fiber Base listening
        self.getFBRiderData()
        self.ListenTripStatus()
        self.listenDriverLocation()
        self.getWalletMoney() // for getting wallet Balance
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
        if timer != nil{
            self.timer.invalidate()
        }
    }
    
    func setupView(){
//        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
//        self.revealViewController().rearViewRevealWidth = 300
        
        // set border for from label
        self.fromAddressLBL.layer.cornerRadius = 20
        self.fromAddressLBL.layer.borderColor = UIColor(named: "AppColor")?.cgColor
        self.fromAddressLBL.layer.borderWidth = 1
        self.fromAddressLBL.layer.masksToBounds = true
        self.fromAddressLBL.text = Localize.stringForKey(key: "where_to")
        
        self.backView.isRoundedView = true
        self.backView.isRoundedBorder = true
        self.backView.layer.masksToBounds = true
        
        self.menusView.layer.cornerRadius = self.menusView.frame.width/2
        self.menusView.isElevation = 3
        self.sourceAddrView.isElevation = 2
        self.sourceAddrView.layer.cornerRadius = 10
        
        self.designationView.isElevation = 2
        self.designationView.layer.cornerRadius = 10
        
        self.requestBtn.roundeCornorBorder = 10
        self.vechileView.leftRightRoundCorners(radius: 10.0)
        
        
        let fillColor = UIColor.lightGray
        let rippleView = SMRippleView(frame: self.rippleView.bounds, rippleColor: UIColor.clear, rippleThickness: 0.15, rippleTimer: 2, fillColor: fillColor, animationDuration: 5, parentFrame: self.sendRequestView.bounds)
        self.rippleView.addSubview(rippleView)
        
    }
    
    func mapPadding(addBottom : CGFloat , reduceBottom : CGFloat){
        
        /* add reduce means we want to set the buttom based on view height and based on screen height
            if we use screen height means we want to add height
            if we use view hight (subviews) we want to reduce the height
         */
        
        switch self.subviewCount {
        case 1:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 150+addBottom-reduceBottom, right: 0)
           
         case 2:
            
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: self.paymentView.frame.height+addBottom-reduceBottom * 1.5, right: 0)
        case 3:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: self.tripView.frame.height+addBottom-reduceBottom+20, right: 0)
        default:
            self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func hideShowView(){
        switch self.hiddenShowViews {
        case 1:
            self.fromAddressLBL.isHidden = false
            self.vechileView.isHidden = true
            self.backView.isHidden = true
            self.menuImg.isHidden = false
            self.menusView.isHidden = false
            self.backView.isHidden = true
        case 2:
            self.fromAddressLBL.isHidden = true
            self.vechileView.isHidden = false
            self.backView.isHidden = false
            self.menuImg.isHidden = true
            self.menusView.isHidden = true
            self.backView.isHidden = false
            self.subviewCount = 1
        case 3 :
            self.fromAddressLBL.isHidden = true
            self.vechileView.isHidden = true
            self.backView.isHidden = true
            self.menuImg.isHidden = true
            self.menusView.isHidden = true
            self.backView.isHidden = true
        default:
            self.fromAddressLBL.isHidden = false
        }
    }
    
    func setupDelegate(){
    }
    
    func setupAction(){
        self.menusView.addAction(for: .tap) {
            if self.revealViewController() != nil {
                self.revealViewController().revealToggle(animated: true)
            }
//            let vc = InvoiceVC.initWithStory()
//            self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
        self.backView.addAction(for: .tap) {
            self.clearView()
        }
        
        self.fromAddressLBL.addAction(for: .tap) {
           if !self.currentAddress.isEmpty{
                self.navigationController?.isNavigationBarHidden = true
                let vc = SearchAddressVC.initWithStory()
                vc.currentAddress = self.currentAddress
                vc.currentLocation = self.currentLocation
                vc.tripRouteDelegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
//                showToast(msg: StringFile.err_getAddress)
                convertLatLangTOAddress(coordinates: self.currentLocation, address: { (address) -> Void in
                                    self.currentAddress = address
                                    print("*****Getting_Address" , self.currentAddress)
                                })
            }
        }
        
        self.requestBtn.addAction(for: .tap) {
            if !(self.requestBtn.title(for: .normal) == self.Localize.stringForKey(key: "no_vehicle")){
                let pickUpCity = self.pickupaddr.components(separatedBy: ",")
            
                switch pickUpCity.count{
                case 3 :
                    self.pickupCity = pickUpCity[1]
                    break
                case 4:
                    self.pickupCity = pickUpCity[2]
                    break
                case 5:
                        self.pickupCity = pickUpCity[3]
                    break
                default :
                        self.pickupCity = pickUpCity[3]
                    break
                }
                if !self.pickupaddr.isEmpty && !self.dropAddr.isEmpty && !self.serviceData._id.isEmpty{
                    
                    self.mapView.settings.myLocationButton = false
                    if let profile : ProfileModel = Constant.profileData{
                        
                    self.paymentView.initView(view: self.view, seriveDetail: self.serviceData,pickupLoc: self.pickupLoc,dropLoc: self.dropLoc,pickupCity: self.pickupCity, requestNow: {(requested,requestType) in
                        self.mapView.settings.myLocationButton = true

                        self.paymentView.deInitView(request: "")
                        print("requestType",requestType)
                        self.requestData = requested
                        if requestType == "rideNow"{
                            
                            self.sendRequestView.isHidden = false
                            self.vechileView.isHidden = true
                        }else{
                            
                            self.sendRequestView.isHidden = true
                        }
                        
//                        self.tripView.initView(view: self.view, tripStatus: {(tripstatus) in
//
//                        })
                    }, requestLater: {(requestedLater) in
                        print("RequestedLater",requestedLater)
                    })
                       
                    }
                    self.subviewCount = 2
                    self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
                }
            }
        }
        self.cashViewBtn.addAction(for: .tap) {
//            if !Constant.profileData.card.last4.isEmpty{
                self.cashCardOption()
//            }
        }
        self.promoBtn.addAction(for: .tap) {
            self.promoCodeView.initView(view: self.view, promo: {(promo) in
                print("PromoCode",promo.uppercased())
            })
        }
        
        self.cancelImg.addAction(for: .tap) {
            if let request : RideRequestModel = self.requestData{
                self.cancelRide(requestId: request.requestDetails)
            }
        }
      
    }
    
    func setupLang(){
      
        if let payment : String = UserDefaults.standard.value(forKey: UserDefaultsKey.payment) as? String ?? ""{
            if !payment.isEmpty{
                if payment == "cash"{
                    self.cashLbl.text = Localize.stringForKey(key: "cash")
                }else{
                    self.cashLbl.text = Localize.stringForKey(key: "card")
                }
                
            }else{
                self.cashLbl.text = Localize.stringForKey(key: "cash")
                UserDefaults.standard.set("cash", forKey: UserDefaultsKey.payment)
            }
        }
        
        self.promoLbl.text = Localize.stringForKey(key: "promo")
        self.requestBtn.setTitle(Localize.stringForKey(key: "no_vehicle"), for: .normal)
        
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
       self.cashCardOption()
    }
    func cashCardOption(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: Localize.stringForKey(key: "cash"), style: .default, handler: {(action) in
            self.cashLbl.text = self.Localize.stringForKey(key: "cash")
            UserDefaults.standard.set("cash", forKey: UserDefaultsKey.payment)
        }))
//        actionSheet.addAction(UIAlertAction(title: Localize.stringForKey(key: "mobile_money"), style: .default, handler: {(action) in
//            self.cashLbl.text = self.Localize.stringForKey(key: "mobile_money")
//            UserDefaults.standard.set(self.Localize.stringForKey(key: "mobile_money"), forKey: UserDefaultsKey.payment)
//         }))
        actionSheet.addAction(UIAlertAction(title: Localize.stringForKey(key: "card"), style: .default, handler: {(action) in
            self.cashLbl.text = self.Localize.stringForKey(key: "card")
            UserDefaults.standard.set("card", forKey: UserDefaultsKey.payment)
        }))
        actionSheet.addAction(UIAlertAction(title: Localize.stringForKey(key: "cancel"), style: .cancel, handler: {(action) in
            
        }))
        if let popview = actionSheet.popoverPresentationController {
            popview.sourceView = cashView
            popview.sourceRect = cashView.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func clearView(){
        self.loadView()
        self.focusZoom = 12
        self.selectedVCIndex = -1
        self.currentAddress = ""
        self.subviewCount  = -1
        self.hiddenShowViews  = 1
        
        self.pickupaddr = ""
        self.pickupLoc  = CLLocation()
        self.dropAddr  = ""
        self.dropLoc = CLLocation()
        
        self.polyline = GMSPolyline()
        self.animationPolyline = GMSPolyline()
        self.path = GMSPath()
        self.animationPath = GMSMutablePath()
        self.i = 0
        
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
        
        self.sendRequestView.isHidden = true
        self.tripView.deInitView()
        self.promoCodeView.deInitView()
        self.paymentView.deInitView(request: "")
        self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.mapView.clear()
        self.vechileView.isHidden = true
        self.tripStatusView.isHidden = true
        self.backView.isHidden = true
        self.viewDidLoad()
    }
    
    
    class func initWithStory()->HomeVC {
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        return vc
    }
}

//Collection View extension
extension HomeVC : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func setupCVDelegate(){
        self.vehicleCollectionView.delegate = self
        self.vehicleCollectionView.dataSource = self
        
        self.vehicleCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count : Int = self.vechileList?.VechileListList.count ?? 0{
            return count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VechileCells", for: indexPath) as! VechileCells
            if selectedVCIndex == indexPath.row{
                cell.vechileView.backgroundColor = UIColor(named: "SelectedVechBG")
//                collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//                collectionView.bringSubviewToFront(cell)
                
                self.getDriverLocation(defaultVehicle: self.vechileList?.VechileListList[selectedVCIndex].type.lowercased() ?? "", updateValue: 0, isAvailable: {(available) in
                    if available{
                        self.requestBtn.backgroundColor = UIColor.AppColors
                    self.requestBtn.setTitle(self.Localize.stringForKey(key: "request_now"), for: .normal)
                    }else{
                    self.requestBtn.setTitle(self.Localize.stringForKey(key: "request_now"), for: .normal)
                        self.requestBtn.backgroundColor = UIColor(named: "SelectedVechBG")
                        }
                    })
                          
                    UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 5, initialSpringVelocity: 0, options: [], animations: {
                    cell.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                })
            }else{
                cell.vechileView.backgroundColor = UIColor(named: "VechileBG")
                cell.transform = .identity
            }
        if let vechile = self.vechileList?.VechileListList[indexPath.row]{
            self.setVechileData(cell: cell, index: indexPath.row, vechileData: vechile)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! VechileCells
        self.selectedVCIndex = indexPath.row
        self.vehicleCollectionView.reloadData()
        self.serviceData = self.vechileList?.VechileListList[indexPath.row] ?? VechileListData()
    }
    
    func setVechileData(cell : VechileCells , index : Int , vechileData : VechileListData){
        cell.vechileName.text = vechileData.type
        var urls : String = ServiceApi.Base_Image_URL+vechileData.file ?? String()
        cell.vechileImage.pin_setImage(from: URL(string: urls))
        
    }
}


//Listen Notification centers
extension HomeVC {
    func observeNotification(){
         NotificationCenter.default.addObserver(self, selector: #selector(closeEstimateView(_:)), name: .closeEstimateFare, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(closeTripStatusView(_:)), name: .closeTripStatusView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addedEstimateFare(_:)), name: .addedEstimateFare, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(requestData(_:)), name: .requestedView, object: nil)
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(requestSend(_:)), name: .rideRequestSend, object: nil)
    }
    
    @objc func requestSend(_ notification : Notification){
//            self.sendRequestView.isHidden = true
    }
    
    @objc func requestData(_ notification : Notification){
        if let data = notification.userInfo as? [String: String]
        {
            for (name, score) in data
            {
                if name == "requestHeight"{
                    let height : CGFloat = CGFloat(Float(score) ?? 0.0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.subviewCount = 3
                        self.mapPadding(addBottom: 0.0, reduceBottom: height)
                    }
                }
            }
        }
    }
    
    @objc func addedEstimateFare(_ notification : Notification){
        if let data = notification.userInfo as? [String: String]
        {
            for (name, score) in data
            {
                if name == "height"{
                    let height : CGFloat = CGFloat(Float(score) ?? 0.0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.mapPadding(addBottom: 0.0, reduceBottom: height)
                    }
                }
            }
        }
    }
    
    @objc func closeEstimateView(_ notification: Notification)
    {
        self.subviewCount = 1
        self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
    }
    
    @objc func closeTripStatusView(_ notification: Notification)
    {
        self.subviewCount = 0
        self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
    }
}


// Map location Function
extension HomeVC : GMSMapViewDelegate, CLLocationManagerDelegate{
   
    // it enable mapdelegate and location button [163 to 185]
    func setupMapDelegate(){
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.compassButton = true
        
        //Enable Location Service in mobile
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
        else
        {
            showToast(msg: Localize.stringForKey(key: "enable_location_service"))
        }
        
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    // MARK : Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            
        case .notDetermined:
            self.locationManager.requestAlwaysAuthorization()
        case .denied:
            print("User denied access to location.")
        }
    }
    
    // MARK: Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    // MARK: update Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last ?? CLLocation()
        print("*****CurrentLocation",self.currentLocation)
        
        // focus to current  location
        self.mapView.camera = GMSCameraPosition(target: self.currentLocation.coordinate, zoom: self.focusZoom, bearing: 0, viewingAngle: 0)
        
        // getting current Address
        convertLatLangTOAddress(coordinates: self.currentLocation, address: { (address) -> Void in
            self.currentAddress = address
            print("*****Getting_Address" , self.currentAddress)
        })
     
        self.locationManager.stopUpdatingLocation()
    }
    
    
    func setPolyLineWithMaker(pickupaddr: String, dropaddr: String,pickupLoc : CLLocation , dropLoc : CLLocation , isRideFlowStated : Bool,tripstatus : String){
        let origin = "\(pickupLoc.coordinate.latitude),\(pickupLoc.coordinate.longitude)"
        let destination = "\(dropLoc.coordinate.latitude),\(dropLoc.coordinate.longitude)"
        if tripstatus == "started"{
            self.isdrawedStartedPolyLine = true
        }else{
            self.isdrawedStartedPolyLine = false
        }
        
        if tripstatus == "accepted"{
                   self.isdrawedAcceptedPolyLine = true
               }else{
                   self.isdrawedAcceptedPolyLine = false
               }
        self.googleVM.getPolylineTimeTravel(origin: origin, destination: destination)
        
        self.googleVM.directionClosure = {
            
            
            self.hiddenShowViews = 2
            self.subviewCount = 1
            self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
            self.hideShowView()
            
            guard let drirection = self.googleVM.getDirection else {return}
           
            self.path = GMSPath(fromEncodedPath:  self.googleVM.getDirection?.routes?[0].overviewPolyline?.points ?? "")!
            self.polyline.path = self.path
            self.polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            self.polyline.strokeWidth = 3.0
            self.polyline.title =  "\(String(describing: self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text))\n\(self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text)"
            self.polyline.map = self.mapView
//            if !isRideFlowStated{
//                self.timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
//            }
            var bounds = GMSCoordinateBounds()
            for index in 1...self.path.count() {
                bounds = bounds.includingCoordinate(self.path.coordinate(at: index))
            }
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds,withPadding: 100))
            let pickupRoadPoin : CLLocation = CLLocation(latitude: Double(self.googleVM.getDirection?.routes?[0].legs?[0].startLocation?.lat ?? 0.0), longitude:  Double(self.googleVM.getDirection?.routes?[0].legs?[0].startLocation?.lng ?? 0.0))
            let dropRoadPoin : CLLocation = CLLocation(latitude: Double(self.googleVM.getDirection?.routes?[0].legs?[0].endLocation?.lat ?? 0.0), longitude:  Double(self.googleVM.getDirection?.routes?[0].legs?[0].endLocation?.lng ?? 0.0))
            
            self.setMaker(pickupaddr: pickupaddr, dropaddr: dropaddr,pickupLoc : pickupRoadPoin ,dropLoc : dropRoadPoin , time : self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 \(Constant.distanceUnit)", distance : self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "o mins", isRideFlowStated: isRideFlowStated)
            
//            self.setInfoWindow(pickupaddr: pickupaddr, dropaddr: dropaddr,pickupLoc : pickupLoc ,dropLoc : dropLoc , time : self.googleVM.getDirection?.routes?[0].legs?[0].distance?.text ?? "0 km", distance : self.googleVM.getDirection?.routes?[0].legs?[0].duration?.text ?? "o mins" )
        }
        
        self.googleVM.errDirectionClouser = {
//            self.hiddenShowViews = 1
//            self.subviewCount = 0
//            self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
//            self.hideShowView()
            self.hiddenShowViews = 2
            self.subviewCount = 1
            self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
            self.hideShowView()
            
        }
    }
    
    @objc func animatePolylinePath() {
        
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 3
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    func setMaker(pickupaddr : String , dropaddr : String,pickupLoc : CLLocation , dropLoc : CLLocation ,time : String , distance : String,  isRideFlowStated : Bool){
        let pickupMarker = GMSMarker()
        pickupMarker.icon = UIImage(named: "pickup_marker.png")
        pickupMarker.map = self.mapView
        pickupMarker.isFlat = true
        pickupMarker.position = CLLocationCoordinate2D(latitude: pickupLoc.coordinate.latitude, longitude: pickupLoc.coordinate.longitude)

        let dropMarker = GMSMarker()
        dropMarker.icon = UIImage(named: "drop_marker.png")
        dropMarker.map = self.mapView
        dropMarker.isFlat = true
        dropMarker.position = CLLocationCoordinate2D(latitude: dropLoc.coordinate.latitude, longitude: dropLoc.coordinate.longitude)
        if !isRideFlowStated{
            self.pickupTagMarker = PickupMarker(location: pickupLoc, address: pickupaddr, timeDistanc: "\(time),\(distance)")
            pickupTagMarker.map = self.mapView
            pickupTagMarker.userData = "pickup"
            pickupTagMarker.groundAnchor = CGPoint(x: -0.01, y: 1.4)
            
            self.dropTagMarker = DropMarker(location: dropLoc, address: dropaddr)
            dropTagMarker.map = self.mapView
            dropTagMarker.userData = "drop"
            dropTagMarker.groundAnchor = CGPoint(x: -0.01, y: 1.4)
        }
    }
    
    func setDriverLocationMarker(driverLoc : FBOfferLocation){
        driverMarker.icon = UIImage(named: "car_maker.png")
        driverMarker.map = self.mapView
        driverMarker.isFlat = true
        if driverLoc.l.count>0{
        driverMarker.position = CLLocationCoordinate2D(latitude: driverLoc.l[0], longitude: driverLoc.l[1])
        }
        driverMarker.rotation = driverLoc.bearing
        
//        if(!isMarkerWithinScreen(marker: driverMarker)){
//            let fancy = GMSCameraPosition.camera(withLatitude: driverLoc.l[0], longitude: driverLoc.l[1], zoom: 10, bearing: driverLoc.bearing, viewingAngle: 0)
//            self.mapView.animate(to: fancy)
//        }
    }
    
    func isMarkerWithinScreen(marker: GMSMarker) -> Bool {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(marker.position)
    }
    
    func setInfoWindow(pickupaddr : String , dropaddr : String,pickupLoc : CLLocation , dropLoc : CLLocation ,time : String , distance : String ){
        self.pickupInfoWindow = PickupLocView().loadView()
        self.pickupInfoWindow?.layer.backgroundColor = UIColor.white.cgColor
        self.pickupInfoWindow?.layer.cornerRadius = 8
        self.pickupInfoWindow?.center = self.mapView.projection.point(for:CLLocationCoordinate2D(latitude: pickupLoc.coordinate.latitude, longitude: pickupLoc.coordinate.longitude))
        self.pickupInfoWindow?.center.y -= 100
        self.pickupInfoWindow?.AddrLbl.text = pickupaddr
        self.pickupInfoWindow?.travelLbl.text = "\(distance) , \(time)"
        self.pickupInfoWindow?.addAction(for: .tap, Action: {
            print("HELOOO")
        })
        self.mapView.addSubview(self.pickupInfoWindow!)

        self.dropInfoWindow = DropLocView().loadView()
        self.dropInfoWindow?.layer.backgroundColor = UIColor.white.cgColor
        self.dropInfoWindow?.layer.cornerRadius = 8
        self.dropInfoWindow?.center = self.mapView.projection.point(for:CLLocationCoordinate2D(latitude: dropLoc.coordinate.latitude, longitude: dropLoc.coordinate.longitude))
        self.dropInfoWindow?.center.y -= 100
        self.dropInfoWindow?.addressLbl.text = dropaddr
        self.dropInfoWindow?.addAction(for: .tap, Action: {
            print("SDSHDLKSD")
        })
        self.mapView.addSubview(self.dropInfoWindow!)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.userData as? String == "pickup"{
             var vc = PinAddressVC.initWithStory()
             vc.tripPinRoute = self
             vc.currentAddress = self.pickupaddr
             vc.currentLocation = self.pickupLoc
             vc.tag = "pickup"
             self.navigationController?.pushViewController(vc, animated: true)
        }else if marker.userData as? String == "drop"{
            var vc = PinAddressVC.initWithStory()
            vc.tripPinRoute = self
            vc.currentAddress = self.dropAddr
            vc.currentLocation = self.dropLoc
            vc.tag = "drop"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        
        if pickupTagMarker != nil{
            let screenSize: CGRect = UIScreen.main.bounds
            let pickupPoint  = self.mapView.projection.point(for: self.pickupLoc.coordinate)
            let markerWindowSize: CGRect = self.pickupTagMarker.view.bounds
            
            let dropPoint  = self.mapView.projection.point(for: self.dropLoc.coordinate)
            let dropmarkerWindowSize: CGRect = self.dropTagMarker.view.bounds
            
            if (screenSize.width-pickupPoint.x-10) < markerWindowSize.width{
                self.pickupTagMarker.groundAnchor = CGPoint(x: 0.99, y: 1.4)
            }else{
                self.pickupTagMarker.groundAnchor = CGPoint(x: -0.01, y: 1.4)
            }
            
            if (screenSize.width-dropPoint.x-10) < dropmarkerWindowSize.width{
                self.dropTagMarker.groundAnchor = CGPoint(x: 0.99, y: 1.4)
            }else{
                self.dropTagMarker.groundAnchor = CGPoint(x: -0.01, y: 1.4)
            }
        }
    }
    
}

//APi call
extension HomeVC{
    
    func getWalletMoney(){
        self.paymentvm.getMyWallet(view: self.view)
      
    }
    
    func getVechileServiceList(pickupLoc: CLLocation, dropLoc: CLLocation){
        self.homevm.getVechileList(view: UIView(), pickupLoc: pickupLoc, dropLoc: dropLoc)
        
        self.homevm.getVechileClosure = {
            self.vechileList = self.homevm.vechileservice
            self.serviceData = self.vechileList?.VechileListList[0] ?? VechileListData()
        }
        
        self.homevm.errgetVechileClosure = {
             self.homevm.getVechileList(view: UIView(), pickupLoc: pickupLoc, dropLoc: dropLoc)
        }
    }
    
    func cancelRide(requestId : String){
        
        self.homevm.cancelRide(view: self.view, requestid: requestId)
        
        self.homevm.getcancelClouser = {
           self.sendRequestView.isHidden = true
            self.requestData = RideRequestModel()
            //self.clearView()
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
            //                 
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        }
        
        self.homevm.errcancelClouser = {
             self.sendRequestView.isHidden = true
            self.requestData = RideRequestModel()
//            self.clearView()
            
        }
        
      
    }
    
    func cancelCurrentTrip(tripID : String){
        self.homevm.cancelCurrentTrip(view: self.view, tripId: tripID)
        
        self.homevm.getcancelTripClouser = {
            self.sendRequestView.isHidden = true
            self.requestData = RideRequestModel()
            self.updateCancelTripSttaus(status: "5")
//            self.clearView()
            let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
            self.appDelegate.window?.rootViewController = MenuRoot
        }
        
        self.homevm.errcancelTripClouser = {
            self.sendRequestView.isHidden = true
        }
    }
    
    func tripDriverDetails(tripID : String,tripstatus : String,fbriderstatus : FBRiderDataModel){
        self.tripView.deInitView()
        self.homevm.tripDriverDetails(view: self.view, tripId: tripID)
        self.homevm.getRideDetailClouser = {
            if let rideDetails : RideDetailModel = self.homevm.rideDetails{
                let driverVehicle : String = rideDetails.serviceType as? String ?? ""
              
                if !driverVehicle.isEmpty{
                    UserDefaults.standard.set(driverVehicle, forKey: UserDefaultsKey.driverVehcile)
                }
                self.mapView.clear()
                self.driverMarker = GMSMarker()
                
                self.listenDriverLocationForPolyline(tripstatus : tripstatus,rideDetails: rideDetails)

                self.tripView.initView(view: self.view, driverDetail: rideDetails, rideStatus: tripstatus, call: { (call) in
                    if let url = URL(string: "tel://\(call)"), UIApplication.shared.canOpenURL(url) {
                        if #available(iOS 10, *) {
                            UIApplication.shared.open(url)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }, message: { (message) in
                    if (MFMessageComposeViewController.canSendText()) {
                        let controller = MFMessageComposeViewController()
                        controller.body = ""
                        controller.recipients = [message]
                        controller.messageComposeDelegate = self
                        self.present(controller, animated: true, completion: nil)
                    }
                }, cancel: { (canel) in
                    let alert = UIAlertController(title:self.Localize.stringForKey(key: "caceltitle") , message: self.Localize.stringForKey(key: "cancel_content"), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "no"), style: UIAlertAction.Style.default, handler: nil))
                    alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "yes"), style: UIAlertAction.Style.default, handler: { (alert) in
                       self.cancelCurrentTrip(tripID: fbriderstatus.current_tripid.description)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }, share: { (share) in
                    let text : String = "Hi , you can use this add from this link -> https://itunes.apple.com/us/app/rebustar-rider/id1414323066?ls=1&mt=8"
                    let vc = UIActivityViewController(activityItems: [text], applicationActivities: nil)
                    vc.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
                    vc.popoverPresentationController?.sourceView = self.tripView.shareView
                    DispatchQueue.main.async {
                        self.present(vc, animated: true, completion: nil);
                    }
                })
            }
        }
        
        self.homevm.errRideDetailClouser = {
            self.sendRequestView.isHidden = true
            self.requestData = RideRequestModel()
            self.clearView()
        }
    }
}

//Firebase Flow
extension HomeVC{
    func getFBRiderData(){
        self.FBCONNECT.getRideFlow { (riderdata) in
            if let fbriderstatus : FBRiderDataModel = riderdata{
                switch riderdata.tripstatus{
                case "Processing":
                    
                    self.sendRequestView.isHidden = false
                    self.vechileView.isHidden = true
                    
                    break
                case "No Driver Found":
                    self.sendRequestView.isHidden = true
                    self.vechileView.isHidden = true
                    self.FBCONNECT.cancelReason(cancelReason: { (cancelReason) in
                        let alert = UIAlertController(title: self.Localize.stringForKey(key: "alert"), message: self.Localize.stringForKey(key: "no_driver"), preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: self.Localize.stringForKey(key: "ok"), style: UIAlertAction.Style.default, handler: {(action) in
                            self.sendRequestView.isHidden = true
                            
                        }))
                         self.present(alert, animated: true, completion: nil)
                      })
                    self.FBCONNECT.clearRiderData()
                    self.clearView()
                    self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    break
                case "Accepted":
                    self.sendRequestView.isHidden = true
                    let tripID : String = (fbriderstatus.current_tripid ?? 0).description ?? ""
                    UserDefaults.standard.set(tripID, forKey: UserDefaultsKey.tripid)
                   UserDefaults.standard.set(fbriderstatus.tripdriver, forKey: UserDefaultsKey.driverid)
                    self.tripDriverDetails(tripID: tripID,tripstatus : "Accepted",fbriderstatus: fbriderstatus)
                    
                    self.tripStatusView.isHidden = false
                    self.tripStatusLbl.text = self.Localize.stringForKey(key: "accepted_request")
                    
                    
                   self.hiddenShowViews = 3
                    self.hideShowView()
                    self.ListenTripStatus()
                    break
                case "Cancelled":
                    self.FBCONNECT.clearRiderData()
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
                    self.clearView()
                    self.viewDidLoad()
                    break
                case "Canceled":
                    self.FBCONNECT.clearRiderData()
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
                    self.clearView()
                    self.viewDidLoad()
                    break
                default :
                    break
                }
            }
        }
    }
    
    
    func updateCancelTripSttaus(status : String){
        self.FBCONNECT.updateCancelTripstatus(status: status)
    }
    
    func listenDriverLocation(){
       
        self.FBCONNECT.listenDriverLocation(driverlocation: {(driverLocation) in
        
            self.setDriverLocationMarker(driverLoc: driverLocation)
        })
    }
    
    func listenDriverLocationForPolyline(tripstatus : String,rideDetails : RideDetailModel){
        var driverLoc : CLLocation = CLLocation()
        self.FBCONNECT.listenDriverLocation(driverlocation: {(driverLocation) in
            if driverLocation.l.count > 0 {
                driverLoc = CLLocation(latitude: driverLocation.l[0] ?? 0.0, longitude: driverLocation.l[1] ?? 0.0)
            }
            
            let startLoc : CLLocation = CLLocation(latitude: CLLocationDegrees(rideDetails.pickupdetails.startcoords[1] ?? 0.0), longitude: CLLocationDegrees(rideDetails.pickupdetails.startcoords[0] ?? 0.0))
            let endLoc : CLLocation = CLLocation(latitude: CLLocationDegrees(rideDetails.pickupdetails.endcoords[1] ?? 0.0), longitude: CLLocationDegrees(rideDetails.pickupdetails.endcoords[0] ?? 0.0))
           print("TRIPSTATSss",tripstatus)
            self.sourceAddrssLbl.text = rideDetails.pickupdetails.start
            self.designLbl.text = rideDetails.pickupdetails.end
            
             if tripstatus == "Accepted"{
                if !self.isdrawedAcceptedPolyLine{
                    self.mapView.clear()
                    self.setPolyLineWithMaker(pickupaddr: "", dropaddr: "", pickupLoc: driverLoc, dropLoc: startLoc, isRideFlowStated: true, tripstatus: "accepted")
                }
            }else if tripstatus == "started"{
            
                if !self.isdrawedStartedPolyLine{
                self.mapView.clear()
                self.setPolyLineWithMaker(pickupaddr: "", dropaddr: "", pickupLoc: driverLoc, dropLoc: endLoc, isRideFlowStated: true,tripstatus: "started")
                               
                }
             }else{
                self.setPolyLineWithMaker(pickupaddr: "", dropaddr: "", pickupLoc: driverLoc, dropLoc: startLoc, isRideFlowStated: true,tripstatus: "started")
                               
                
            }
        })
    }
    
    func ListenTripStatus(){
        self.FBCONNECT.getTripData { (tripDatas) in
            if let fbtripDatas : FBTripDataModel = tripDatas{
                switch fbtripDatas.status{
                case "1":
                    self.tripStatusView.isHidden = false
                    self.tripStatusLbl.text = self.Localize.stringForKey(key: "accepted_request")
                 
                   self.listenDriverLocation()
                    break
                case "2":
                    self.tripStatusView.isHidden = false
                     self.tripStatusLbl.text = self.Localize.stringForKey(key: "driver_arrived")
                    self.listenDriverLocation()
                    let tripID : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? ""
                    
                  
                    break
                case "3":
                    self.tripStatusView.isHidden = false
                    self.tripStatusLbl.text = self.Localize.stringForKey(key: "trip_started")
                    let tripID : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? ""
                    self.listenDriverLocation()
                    self.vechileView.isHidden = true
                    self.FBCONNECT.getRideFlow { (riderdata) in
                        if let fbriderstatus : FBRiderDataModel = riderdata{
                            self.tripDriverDetails(tripID: tripID,tripstatus : "started",fbriderstatus: fbriderstatus)
                        }
                    }
                    break
                case "4":
                    self.tripStatusView.isHidden = true
                    self.vechileView.isHidden = true
//                    self.tripStatusLbl.text = self.Localize.stringForKey(key: "trip_ended")
                    self.mapView.clear()
                    self.tripView.removeFromSuperview()
                    self.tripView.isHidden = true
                    self.hiddenShowViews = 1
                    self.subviewCount = 0
                    self.mapPadding(addBottom: 0.0, reduceBottom: 0.0)
                    self.hideShowView()
                    
                    ShowMsginWindow.instanse.LoadingShow(view: self.view)
                    let vc = InvoiceVC.initWithStory()
                    vc.triproute  = self
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.present(vc, animated: true, completion: nil)
                    
                    ShowMsginWindow.instanse.LoadingHide(view: self.view)
                    break
                case "5":
                    self.FBCONNECT.clearRiderData()
                    self.vechileView.isHidden = true
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
                    UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
//                    self.clearView()
//                    self.viewDidLoad()
                    let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
                    self.appDelegate.window?.rootViewController = MenuRoot
                    break
                default :
                    break
                }
            }
        }
    }
}



extension HomeVC{
   func getDriverLocation(defaultVehicle : String , updateValue : Int , isAvailable : @escaping(Bool)->()){
            var data = updateValue
            let geofireRef = Database.database().reference().child("drivers_location").child(defaultVehicle)
            let geoFire = GeoFire(firebaseRef: geofireRef)
            let center = CLLocation(latitude:self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
            var circleQuery = geoFire.query(at: center, withRadius: 10.0)
            circleQuery.observe(.keyEntered, with: {(key,location) in
                print("ƒƒƒƒƒKey'\(key)' in area at location '\(location)'")
                           data = 1
                           self.setNearbyDriverLocationMarker(key: key, driverLoc: location)
            })
          
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                if data == 0{
                    if self.driverMarkers.count > 0{
                        for value in 0...self.driverMarkers.count-1{
                            self.driverMarkers[value].map = nil
                            self.driverMarkers.remove(at: value)
                            self.geoLocation.remove(at: value)
                            self.geokey.remove(at: value)
                        }
                    }
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                if self.driverMarkers.count > 0{
                    isAvailable(true)
                }else{
                    isAvailable(false)
                }
            })
           
        }
        func setNearbyDriverLocationMarker(key : String ,driverLoc : CLLocation){
            var changeINT : Int = -1
            if self.geokey.count > 0{
                for value in 0...self.geokey.count-1{
                    if self.geokey.contains(key){
                        let index : Int = self.geokey.index(of: key) ?? -1
                        self.geoLocation.remove(at: index)
                        changeINT = index
                        self.geoLocation.insert(driverLoc, at: index)
                        let marker = GMSMarker()
                        marker.icon = UIImage(named: "car_maker.png")
                        marker.title = key
                        marker.isFlat = true
                        marker.position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
                        for value in self.driverMarkers{
                            if (value.title ?? "") == self.geokey[index]{
                                value.position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
                                self.driverMarkers[index].position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
                            }
                        }
                    }else{
                        self.geokey.append(key)
                        self.geoLocation.append(driverLoc)
                        let marker = GMSMarker()
                        marker.icon = UIImage(named: "car_maker.png")
                        marker.title = key
                        marker.isFlat = true
                        marker.position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
                        self.driverMarkers.append(marker)
                        marker.map = mapView
                    }
                }
            }else{
                self.geokey.append(key)
                self.geoLocation.append(driverLoc)
                let marker = GMSMarker()
                marker.icon = UIImage(named: "car_maker.png")
                marker.title = key
                marker.isFlat = true
                marker.position = CLLocationCoordinate2D(latitude: driverLoc.coordinate.latitude, longitude: driverLoc.coordinate.longitude)
                self.driverMarkers.append(marker)
                marker.map = mapView
            }
            print("selfasdasd",self.geoLocation.count , self.geokey)
            //   self.setArrayMarker(changeMarker: self.driverMarkers, indexToChange :changeINT )
        }
        func setArrayMarker(changeMarker : [GMSMarker],indexToChange : Int){
            for value in changeMarker{
                if indexToChange >= 0{
                    if (value.title ?? "") == self.geokey[indexToChange]{
                        value.position = CLLocationCoordinate2D(latitude: self.geoLocation[indexToChange].coordinate.latitude, longitude: self.geoLocation[indexToChange].coordinate.longitude)
                    }
                }
            }
        }

    }

