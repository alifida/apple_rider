//
//  InvoiceVC.swift
//  RebuStar Driver
//
//  Created by Abservetech on 15/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import UIKit
import Cosmos

class InvoiceVC: UIViewController {
    
    @IBOutlet weak var priceTopView: UIView!
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var priceTable: UITableView!
    
    @IBOutlet weak var PriceTabeleHeight: NSLayoutConstraint!
    @IBOutlet weak var pageTitleLbl: UILabel!
    @IBOutlet weak var totalFeeLbl: UILabel!
    
    @IBOutlet weak var commentTxt: UITextField!
    @IBOutlet weak var ratingTitle: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    @IBOutlet weak var startRatingView: CosmosView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var dropAddress: UILabel!
    @IBOutlet weak var currentAddrssLbl: UILabel!
    @IBOutlet weak var billLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dicountLbl: UILabel!
    @IBOutlet weak var discountPriceLbl: UILabel!
    
    
    //VariableDeclaraction
    let Localize : Localizations = Localizations.instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
   
    var homevm = HomeVM()
    var ratings : String = ""
    var triproute : TripRoutes?
    
    //Firebase object
    var FBConnect = FireBaseconnection.instanse
    
    var priceTitleArray : [String] = []
    var priceValueArray : [String] = []
    var paymentType : String = ""
    var priceStr : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        self.getFBDriverDetails()
        self.homevm = HomeVM(view: self.view, dataService: ApiRoot())
        
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        self.setupView()
        self.setupAction()
        self.setupLang()
        self.setupDelegate()
    }
    
    func setupView(){
        self.priceTopView.layer.cornerRadius = 10
        self.priceTopView.isElevation = 3
        self.addressView.layer.cornerRadius = 10
        self.addressView.isElevation = 3
        self.submitBtn.roundeCornorBorder = 5
        self.priceTable.isElevation = 3
        self.addressView.layer.cornerRadius = 10
        self.priceTable.layer.cornerRadius = 10
        
        commentTxt.placeholder = Localize.stringForKey(key: "enter_comments")
        
    }
    
    func setupAction(){
        
        self.startRatingView.didFinishTouchingCosmos = { rating in
            self.ratings = rating.description
        }
        
        self.submitBtn.addAction(for: .tap) {
//            self.dismiss(animated: true, completion: nil)
           
            self.ratings = self.startRatingView.rating.description
            
            var comment : String = self.commentTxt.text ?? ""
            if self.paymentType == "cash" || self.paymentType == "CASH" || self.paymentType == "Cash" || self.paymentType.caseInsensitiveCompare("card") == .orderedSame {
                self.riderFeedback(rating: self.ratings, comment: comment)
            }else{
                let vc = TripPaymentVC.initWithStory()
                vc.money = self.priceStr
                self.present(vc, animated: true, completion: nil)
//                self.navigationController?.present(vc, animated: true, completion: nil)
            }
          
            
//            if (!comment.isEmpty && !self.ratings.isEmpty){
//                self.riderFeedback(rating: self.ratings, comment: comment)
//            }else{
//                showToast(msg: "Must give Star Rating and comments for you Driver")
//            }
        }
    }
    
    func setupLang(){
        self.pageTitleLbl.text = Localize.stringForKey(key: "invoice_title")
        self.totalFeeLbl.text = Localize.stringForKey(key: "total_fare")
        self.billLbl.text = Localize.stringForKey(key: "bill_rate")
        self.dicountLbl.text = Localize.stringForKey(key: "discount_app")
        self.ratingTitle.text = Localize.stringForKey(key: "rate_title")
        self.submitBtn.setTitle(Localize.stringForKey(key: "submit"), for: .normal)
    }
    
    class func initWithStory()->InvoiceVC{
        let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        return vc
    }
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.PriceTabeleHeight?.constant = self.priceTable.contentSize.height
    }
    
    func setupDate(tripDetails : FBTripDataModel){
        if let details :FBTripDataModel = tripDetails{
            self.currentAddrssLbl.text = details.pickup_address
            self.dropAddress.text = details.Drop_address
            self.totalPriceLbl.text = Constant.priceTag + details.total_fare
            self.discountPriceLbl.text = Constant.priceTag + details.discount
            self.priceStr = details.total_fare
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd"
            let date : String = formatter.string(from: Date())
            
            self.dateLbl.text = date
            
            self.priceTitleArray = [self.Localize.stringForKey(key: "distance") ,
                                    self.Localize.stringForKey(key: "time") ,
                                    self.Localize.stringForKey(key: "base_fare") ,
                                    self.Localize.stringForKey(key: "Waiting_Time") ,
                                    self.Localize.stringForKey(key: "waiting_fare") ,
                                    self.Localize.stringForKey(key: "time_fare") ,
                               //     self.Localize.stringForKey(key: "ride_fare") ,
                                    self.Localize.stringForKey(key: "pickup_fee") ,
                                    self.Localize.stringForKey(key: "distance_fare") ,
                                    self.Localize.stringForKey(key: "access_fee") ,
                                    self.Localize.stringForKey(key: "cancelleantion_fee") ,
                                    self.Localize.stringForKey(key: "payment_method")]
            
            
            
            self.priceValueArray = [
                details.distance + " \(Constant.distanceUnit)",
                details.time + " Mins",
                Constant.priceTag + details.basefare,
                  details.waitingTime + " Mins",
                  decimalDataString(data : details.waiting_fare),
                  decimalDataString(data : details.time_fare),
                 // decimalDataString(data : details.total_fare),
                  decimalDataString(data : details.convance_fare),
                  decimalDataString(data : details.distance_fare) ,
                  decimalDataString(data : details.tax),
                  decimalDataString(data : details.cancel_fare),
                details.pay_type
            ]
            
            self.paymentType = details.pay_type
//            if details.isWaiting == "0"{
//                self.priceValueArray.remove(at: 4)
//                self.priceTitleArray.remove(at: 4)
//            }
            
            
            
//            if details.trip_type != "flatrate"{
//
//                self.priceValueArray.remove(at: 7)
//                self.priceTitleArray.remove(at: 7)
//
//
//                self.priceValueArray.remove(at: 8)
//                self.priceTitleArray.remove(at: 8)
//
//
//                self.priceValueArray.remove(at: 9)
//                self.priceTitleArray.remove(at: 9)
//            }else{
//                if details.tax == "0"{
//                    self.priceValueArray.remove(at: 9)
//                    self.priceTitleArray.remove(at: 9)
//                }
//                if details.isPickup == "0"{
//                    self.priceValueArray.remove(at: 7)
//                    self.priceTitleArray.remove(at: 7)
//                }
//            }
            
            print("PRICETABLEDATA",self.priceValueArray)
            self.priceTable.reloadData()
        }
    }
}


extension InvoiceVC: UITableViewDataSource,UITableViewDelegate{
    
    func setupDelegate(){
        self.priceTable.register(UINib(nibName: "PriceCell", bundle: nil), forCellReuseIdentifier: "PriceCell")
        self.priceTable.delegate = self
        self.priceTable.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.priceTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceCell", for: indexPath) as! PriceCell
        cell.priceTitlelbl.text = self.priceTitleArray[indexPath.row]
        if self.priceTitleArray.count == self.priceValueArray.count{
            cell.priceLbl.text = self.priceValueArray[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}


extension InvoiceVC{
    func riderFeedback(rating : String , comment : String){
        let tripid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? ""
        if tripid.isEmpty{
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
                self.appDelegate.window?.rootViewController = MenuRoot
            })
        }else{
        self.homevm.riderFeedBack(view: self.view, tripId: tripid, rating: rating, comments: comment)
        self.homevm.getfeedbackClouser = {
            showToast(msg: self.homevm.feedback?.message ?? "")
            self.FBConnect.clearRiderData()
            self.triproute?.clearMapView()
            
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
            UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
                self.appDelegate.window?.rootViewController = MenuRoot
            })
        }
        }
    }
}

extension InvoiceVC{
    func getFBDriverDetails(){
        ShowMsginWindow.instanse.LoadingShow(view: self.view)
        self.FBConnect.getTripData { (tripDetails) in
            self.setupDate(tripDetails: tripDetails)
            ShowMsginWindow.instanse.LoadingHide(view: self.view)
        }
         
    }
}
