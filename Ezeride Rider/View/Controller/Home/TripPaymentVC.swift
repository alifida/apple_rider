//
//  TripPaymentVC.swift
//  PapaTaxi Rider
//
//  Created by Abservetech on 13/02/20.
//  Copyright © 2020 Abservetech. All rights reserved.
//

import UIKit
import Lottie
import WebKit
import Alamofire

class TripPaymentVC: UIViewController,WKNavigationDelegate {
    
    var webView = WKWebView()
    
    @IBOutlet weak var doneBtn : UIButton!
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var paymentView : UIView!
    @IBOutlet weak var headerView : UIView!
    
    var money : String = "0"
    //VariableDeclaraction
     let Localize : Localizations = Localizations.instance
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //Firebase object
     var FBConnect = FireBaseconnection.instanse
    
    override func viewDidLoad() {
        super.viewDidLoad()
           let mobile = Constant.profileData.phone
        //        self.paymentvm.weeCash(view : self.view , rechargeAmount: rechargeAmount ,mobile :mobile)
               let tripid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? ""
                self.webView = WKWebView(frame: CGRect(x: 10, y: 100, width: self.paymentView.frame.size.width-20, height: self.paymentView.frame.size.height-120))
                self.paymentView.addSubview(webView)
                webView.navigationDelegate = self
                ShowMsginWindow.instanse.LoadingShow(view: self.paymentView)
                
                if  NetworkReachabilityManager()!.isReachable {
                    ShowMsginWindow.instanse.LoadingHide(view: self.paymentView)
                    let url = URL(string: ServiceApi.mobileMoney + "/\(mobile)/\(money)/tripCharges/\(tripid)")
                    print("ssss\(ServiceApi.mobileMoney + "/\(mobile)/\(money)")")
                    webView.load(URLRequest(url: url!))
                     self.view.isHidden = false
                    self.webView.isHidden = false
                    
                }else{
                    ShowMsginWindow.instanse.LoadingHide(view: self.view)
                    self.webView.isHidden = true
                     
                }
        
        self.backBtn.addAction(for: .tap) {
            self.dismiss(animated: true, completion: nil)
        }
        
        self.doneBtn.addAction(for: .tap) {
             self.FBConnect.clearRiderData()
              //self.triproute?.clearMapView()
              
              UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverid)
              UserDefaults.standard.set(nil, forKey: UserDefaultsKey.driverVehcile)
              UserDefaults.standard.set(nil, forKey: UserDefaultsKey.tripid)
              DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
                  let MenuRoot = SWRevealViewController(rearViewController: MenuVC.initWithStory(), frontViewController: UINavigationController(rootViewController: HomeVC.initWithStory()))
                  self.appDelegate.window?.rootViewController = MenuRoot
        })
        }
                
    }
    
    class func initWithStory()->TripPaymentVC{
           let vc = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "TripPaymentVC") as! TripPaymentVC
           return vc
       }

}
