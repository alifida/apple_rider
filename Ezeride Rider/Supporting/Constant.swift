//
//  Constant.swift
//  RebuStar Rider
//
//  Created by Abservetech on 30/05/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation

// here we have save baseurls , userdefaults , coredata objects , some constants keys

struct ServiceApi{
    static let Base_URL = "https://ezeride.com.au:3001/api/"
    static let Base_Image_URL = "https://ezeride.com.au:3001/"
    static let help = "https://ezeride.com.au:3001/api/webView/helpCategory"
    static let tc = "https://ezeride.com.au:3001/api/tnc"
    static let googleNearbyAddr = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    static let googleDirection = "https://maps.googleapis.com/maps/api/directions/json"
    static let googleAutoComplete = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
    static let googlelatAdd = "https://maps.googleapis.com/maps/api/geocode/json"
    static let login = ServiceApi.Base_URL + "riderslogin"
    static let otpVerification = ServiceApi.Base_URL + "verifyNumber"
    static let riderForgotPassword = ServiceApi.Base_URL + "riderForgotPassword"
    static let signup = ServiceApi.Base_URL + "riders"
    static let profile = ServiceApi.Base_URL + "rider"
    static let pasttrip = ServiceApi.Base_URL + "riderTripHistory"
    static let upcoming_trip = ServiceApi.Base_URL + "riderUpcomingScheduleTaxi"
    static let emerygencyList = ServiceApi.Base_URL + "rideremgcontact"
    static let favAddress = ServiceApi.Base_URL + "ridersAddress"
    static let tripDetail = ServiceApi.Base_URL + "pastTripDetailRider"
    static let vehicleServe = ServiceApi.Base_URL + "serviceBasicFare"
    static let estimationFare = ServiceApi.Base_URL + "estimationFare"
    static let requestTaxi = ServiceApi.Base_URL + "requestTaxi"
    static let cancelTaxi = ServiceApi.Base_URL + "cancelTaxi"
    static let cancelCurrentTrip = ServiceApi.Base_URL + "cancelCurrentTrip"
    static let tripDriverDetails = ServiceApi.Base_URL + "tripDriverDetails"
    static let riderFeedback = ServiceApi.Base_URL + "riderFeedback"
    static let addCard = ServiceApi.Base_URL + "addCard"
    static let addToMyWallet = ServiceApi.Base_URL + "addToMyWallet"
    static let mobileMoney = ServiceApi.Base_URL + "mobileMoney"
    static let myWallet = ServiceApi.Base_URL + "myWallet"
    static let riderpwd = ServiceApi.Base_URL + "riderpwd"
    static let myWalletHistory = ServiceApi.Base_URL + "myWalletHistory"
    static let userCancelScheduleTaxi = ServiceApi.Base_URL + "userCancelScheduleTaxi"
    static let contactUs = ServiceApi.Base_URL + "contactUs/"
    
    static let offersList = ServiceApi.Base_URL + "offersList"

    static let validatePromo = ServiceApi.Base_URL + "validatePromo"
}

struct UserDefaultsKey{
    static let fcmtoken = "FCMTOKEN"
    static let deviceToken = "Device_Token"
    static let promo = "promo"
    static let token = "token"
    static let email = "email"
    static let name = "name"
    static let userid = "userid"
    static let loginstatus = "loginstatus"
    static let language = "language"
    static let payment = "payment"
    static let tripid = "trip_id"
    static let driverid = "driver_id"
    static let tripstated = "tripstarted"
    static let driverVehcile = "driverVehcile"
}


struct Constant {
    static var priceTag = "AUD "
    static var phoneCode = "+61"
    static var distanceUnit = "Km"
    static var googleAPiKey = "AIzaSyCvr9noGfMRsMf9jHgM4QWiU9sgoaYscFM"
    static var countryCode = ((Locale.current as NSLocale).object(forKey: .countryCode) as? String)?.lowercased() ?? "in"
    static var profileData = ProfileModel()
    static var stripkey = "pk_live_51GKLvzCnZxF8WZy8TYjpAs2TX8eEEZwe18Ff5nzc4MaSBEpUDTv13ClxkH8TXcDabDtZJrOINs9g0sDBDJS5qdn400niMB6KiN"
    static var walletMoney = "0"
}

struct StringFile {
    static let errorTitle = "Error"
    static let emailError = "Please Enter Valid Email Id"
    static let passwordError = "Please Enter Valid Password"
    static let phonenumError = "Please Enter Mobile number with 10 digits only"
    static let somethingwrong = "Something went wrong"
    static let networkerror = "Please Check your InterNet Connection"
    static let timeouterror = "We could not connect with you , so please try again later"
    static let err_name = "Please enter your name"
    static let err_contactnum = "Please enter your Contact number"
    static let err_flat = "Please enter your Flat no:"
    static let err_area = "Please enter your Area"
    static let err_landmark = "Please enter your Landmark"
    static let err_city = "Please enter your city"
    static let err_address = "Please select the address"
    static let err_slot = "Please select the slot to delivery"
    static let err_getAddress = "Sorry!!! con't get Address"
    static let err_network = "Please check with your internet connection !!!"
    static let err_upload = "Sorry con't upload data"
    
}

extension Notification.Name {
    static let closeEstimateFare = Notification.Name("closeEstimateFare")
    static let addedEstimateFare = Notification.Name("addedEstimateFare")
    static let closeTripStatusView = Notification.Name("closeTripStatusView")
    static let requestedView = Notification.Name("requestedview")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
    static let rideRequestSend = Notification.Name("rideRequestSend")
}

