//
//  FireBaseconnection.swift
//  RebuStar Rider
//
//  Created by Abservetech on 03/07/19.
//  Copyright © 2019 Abservetech. All rights reserved.
//

import Foundation
import FirebaseDatabase
import CoreLocation
import GooglePlaces

import Alamofire
import SwiftyJSON

class FireBaseconnection{
    
    static let instanse = FireBaseconnection()
    let fireBaseref : DatabaseReference = Database.database().reference()
    
    var userid : String = ""
    init(){
        self.userid = UserDefaults.standard.string(forKey: UserDefaultsKey.userid) ?? ""
    }
    
    func updateGeoFire(loc : CLLocation , tripId : String )  {
//        let fireBase : DatabaseReference = Database.database().reference().child("offer_ride_location").child(tripId)
//        let geoFire = GeoFire(firebaseRef: fireBase)
//
//        geoFire.setLocation(loc, forKey: self.userid) { (error) in
//            if (error != nil) {
//                print("An error occured: \(String(describing: error))")
//            } else {
//                print("Saved location successfully!")
//            }
//        }
    }
    
    func removerGeoFrie(tripId : String){
        let fireBase : DatabaseReference = Database.database().reference().child("offer_ride_location").child(tripId).child(self.userid)
        fireBase.removeValue()
        
    }
    
    func addUserDetail(userid : String , name : String , responsibleUserID : String ,status : String ){
        let userArray = [
            "name": name ,
            "responsibleUserID": responsibleUserID ,
            "status": status
        ]
        fireBaseref.child("userData").child(self.userid).updateChildValues(userArray)
    }
    
    func observerUserStatusForInvite(userId : String , success : @escaping(String)->()) {
//        fireBaseref.child("userData").child(userId).observe(.value) { (snapShot) in
//            let userdata = snapShot.value
//            let json = JSON(snapShot.value)
//            print("userFBjson", json)
//            let usersatus = FBUserData.init(json: json)
//            success(usersatus.status)
//        }
    }
    
    func cancelReason(cancelReason : @escaping(FBCancelReason)->()){
        fireBaseref.child("Cancel_reason").observe(.value) { (snapShot) in
            let cancelreason = JSON(snapShot.value)
            let reason = FBCancelReason.init(json: cancelreason)
            print("cancelJSONREason",reason.alertLabels.DRIVER_NOT_FOUND)
            cancelReason(reason)
        }
    }
    
    
    
    func getRideFlow(riderData : @escaping(FBRiderDataModel)->()){
        if let userids : String = self.userid{
            fireBaseref.child("riders_data").child(self.userid).observe(.value) { (snapShot) in
                let riderdata = JSON(snapShot.value)
                print("^^^^^FBRequestData",riderdata)
                FBRiderDataModel.init(json: riderdata)
                riderData(FBRiderDataModel.init(json: riderdata))
            }
        }
    }
    
    //Update drver datas after he signup
    func clearRiderData(){
        
        if let id : String = self.userid{
            if !id.isEmpty{
                let riderArray = [
                    "cancelExceeds": "0" ,
                    "lastCanceledDate": "0",
                    "current_tripid": "0",
                    "tripstatus": "0",
                    "requestId": "0",
                    "tripdriver": "0"
                ]
                fireBaseref.child("riders_data").child(self.userid).updateChildValues(riderArray)
                
                
            }
        }
    }
    
    func updateCancelTripstatus(status : String){
        let tripid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? ""
        if let id : String = tripid{
            if !id.isEmpty{
                let tripstatus = [
                    "status": status
                ]
                fireBaseref.child("trips_data").child(tripid).updateChildValues(tripstatus)

                self.clearRiderData()
            }
        }
    }
    
    
    func listenDriverLocation(driverlocation : @escaping(FBOfferLocation)->()){
        let driverid : String = UserDefaults.standard.value(forKey: UserDefaultsKey.driverid) as? String ?? ""
        let sertviceType : String = (UserDefaults.standard.value(forKey: UserDefaultsKey.driverVehcile) as? String ?? "").lowercased()
        if let id : String = driverid, let sertvice : String = sertviceType{
            if !id.isEmpty && !sertvice.isEmpty{
                if let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? "" {
                    if !trip_id.isEmpty {
                        fireBaseref.child("drivers_location").child("trip_location").child(driverid).observe(.value) { (snapShot) in
                            let json = JSON(snapShot.value)
                            print("^^^^^FBDriverLocationss",json)
                            let driverlocations = FBOfferLocation.init(json: json)
                            driverlocation(driverlocations)
                        }
                    }
                }else{ fireBaseref.child("drivers_location").child(sertviceType).child(driverid).observe(.value) { (snapShot) in
                    let json = JSON(snapShot.value)
                    print("^^^^^FBDriverLocationss",json)
                    let driverlocations = FBOfferLocation.init(json: json)
                    driverlocation(driverlocations)
                    }
                }
            }
        }
    }
    
    func getTripData(tripdata : @escaping(FBTripDataModel)->()){
        if let id : String = self.userid{
            if !id.isEmpty{
                if let trip_id : String = UserDefaults.standard.value(forKey: UserDefaultsKey.tripid) as? String ?? "" {
                    if !trip_id.isEmpty {
                        fireBaseref.child("trips_data").child(trip_id).observe(.value) { (snapShot) in
                            let tripData = JSON(snapShot.value)
                            print("^^^^^FBTRIPData",tripData)
                            
                            let reason = FBTripDataModel.init(json: tripData)
                            tripdata(reason)
                        }
                    }
                }
            }
        }
    }

}
